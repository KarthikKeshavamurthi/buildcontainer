#!/bin/sh
sudo docker stop buildContainer
sudo docker container prune -f

sudo nvidia-docker run --gpus all --runtime nvidia --name buildContainer --net="host" --ipc=host --privileged -v ~/dev/:/root/git/ -v /usr/local/cuda:/usr/local/cuda -v /usr/local/cuda-11:/usr/local/cuda-11 -v /usr/local/cuda-11.6:/usr/local/cuda-11.6 -dt build-container
