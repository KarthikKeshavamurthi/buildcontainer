#!/bin/bash

alias buildShared="sudo docker exec buildContainer /root/buildShared.sh"
alias buildMobility="sudo docker exec buildContainer /root/buildMobility.sh"
alias buildAutonomy="sudo docker exec buildContainer /root/buildAutonomy.sh"
alias buildHealthMonitor="sudo docker exec buildContainer /root/buildHealthMonitor.sh"
alias buildBoltPerception="sudo docker exec buildContainer /root/buildBoltPerception.sh"
alias startBuildContainer="source /home/karthik/dev/buildcontainer/runContainer.sh"

