
# Build on top of ROS melodic
FROM osrf/ros:melodic-desktop-full

# install ssh client and git
RUN apt-get update && apt-get install -y openssh-server git

# Xenial's base image doesn't ship with sudo.
RUN apt-get update && apt-get install -y sudo && rm -rf /var/lib/apt/lists/*
#############################################################################
# Add ros keys to container for installing dependencies
# 1. Setup source list
RUN sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

# 3. Update APt get
RUN sudo apt-get update

# 4. Add dependencies
RUN sudo apt install apt-utils python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential -y

#############################################################################
# execute ROS entry point
RUN /bin/bash -c "source ros_entrypoint.sh"

############################################################################
# Create ROS  workspace and copy files

# Make the catkin dir
RUN /bin/bash -c "mkdir -p /root/catkin_ws/src"

# Perform catkin make
RUN /bin/bash -c '. /opt/ros/melodic/setup.bash; cd /root/catkin_ws; catkin_make'

# Add the workspace to bashrc
RUN /bin/bash -c "sudo echo \"source /root/catkin_ws/devel/setup.bash\" >> ~/.bashrc"

##############################################################################
# Install text editors of everyone's choice (eventhough vim is the best)
RUN /bin/bash -c "sudo apt-get update; sudo apt-get install -y nano vim"

######################## Building Shared ####################################
# Install 3rd party packages.
RUN apt-get update && DEBIAN_FRONTEND="noninteractive" TZ="America/New_York" apt-get -y install tzdata
RUN apt-get update && apt-get -y install cmake curl g++ gcc git libatlas-base-dev libavcodec-dev libavfilter-dev libavformat-dev libavresample-dev libavutil-dev libboost-atomic-dev libboost-chrono-dev libboost-date-time-dev libboost-filesystem-dev libboost-random-dev libboost-regex-dev libboost-serialization-dev libboost-system-dev libboost-thread-dev libcurl4-openssl-dev libdc1394-22-dbg libeigen3-dev libevent-dev libffi-dev libgstreamer-plugins-base1.0-dev libgtk2.0-dev libjpeg-dev liblapack-dev libmysqlclient-dev libncurses5-dev libncursesw5-dev libopenblas-dev libopencore-amrnb-dev libopencore-amrwb-dev libopenexr-dev libpng-dev libsdl2-dev libssl-dev libswscale-dev libtbb-dev libtheora-dev libtiff5-dev libvips libvips-dev libvorbis-dev libwebsocketpp-dev libx264-dev libxvidcore-dev minicom mosquitto ninja-build ntp openssl pkg-config sphinx-common yasm libglew-dev freeglut3-dev gcovr wget autoconf-archive libglfw3-dev libglm-dev

# Setup Creds to use IAMR Nexus
ENV WGETRC="/tmp/.wgetrc"
COPY ./.wgetrc /tmp/.wgetrc
COPY ./iamr-robotics.list /etc/apt/sources.list.d/iamr-robotics.list
COPY ./iamrobotics_auth.conf /etc/apt/auth.conf.d/iamrobotics_auth.conf
RUN wget https://release.iamrobotics.com/repository/IAM-Support/iam-official.key && apt-key add iam-official.key

# Install IAMR specific packages
RUN apt-get update && apt-get -y install thirdparty.iamrobotics.amqpcpp thirdparty.iamrobotics.cpprest thirdparty.iamrobotics.jsoncpp thirdparty.iamrobotics.libgpiod thirdparty.iamrobotics.libmodbus thirdparty.iamrobotics.miniaudio thirdparty.iamrobotics.mqttc=0-Development.d2baba6 thirdparty.iamrobotics.mqttcpp thirdparty.iamrobotics.openni thirdparty.iamrobotics.sicksafetybase thirdparty.iamrobotics.copleycan
ENV IAM_OPENNI2='/usr/local/include/OpenNI-Linux-x64-2.3.0.63'

#install gtest
RUN wget https://github.com/google/googletest/archive/release-1.11.0.tar.gz && tar xzf release-1.11.0.tar.gz && cd googletest-release-1.11.0/ &&  mkdir build && cd build && cmake .. && make -j && make install

# Update shared libs
RUN ldconfig


COPY buildScripts/ /root/
